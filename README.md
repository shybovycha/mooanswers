## MooAnswers

### Brief overview

This is a questions-an-answers web service. Built on top of Laravel PHP framework.

### Installation

First of all, install **[Composer]**:

Under Linux:

    $ curl -sS https://getcomposer.org/installer | php
    $ mv composer.phar /usr/local/bin/composer

Under Windows: [Composer Windows installation manual]

Now, install project dependencies: `php composer.phar install`. And do not forget to add permissions to your `pblic/` and `app/storage/` directories: `chmod -R a+rwx public/ app/storage/`.

Then, set up the MySQL server and configure it at `app/config/database.php` file. At MySQL paragraph.

And finally, update your DB schema running `php artisan migrate:install && php artisan migrate`.

Now, just run `php -S localhost:3000` at the document root of the project and navigate to [http://localhost:3000/server.php/].

More on Laravel installation - [here](http://laravel.com/docs/installation).

  [Composer]: http://getcomposer.org/
  [Composer Windows installation manual]: http://getcomposer.org/doc/00-intro.md#installation-windows
  [http://localhost:3000/server.php/]: http://localhost:3000/server.php/
