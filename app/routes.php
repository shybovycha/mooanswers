<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'UsersController@index');

Route::get('register', 'UsersController@create');
Route::get('log_in', 'UsersController@logIn');
Route::post('log_in', 'UsersController@signIn');
Route::get('log_out', 'UsersController@signOut');

Route::get('users', 'UsersController@index');
Route::get('users/{id}', 'UsersController@show');
Route::get('users/{id}/edit', 'UsersController@edit');
Route::post('users/{id}', 'UsersController@update');
Route::post('users', 'UsersController@store');

Route::get('befriend/{friend_id}', 'UsersController@befriend');
Route::get('unfriend/{friend_id}', 'UsersController@unfriend');

Route::get('ask', 'QuestionsController@create');
Route::post('ask', 'QuestionsController@store');
Route::get('questions', 'QuestionsController@index');
Route::get('questions/{id}', 'QuestionsController@show');
Route::get('questions/{id}/edit', 'QuestionsController@edit');
Route::post('questions/{id}', 'QuestionsController@update');

Route::get('answer/{question_id}', 'AnswersController@create');
Route::post('answer/{question_id}', 'AnswersController@store');