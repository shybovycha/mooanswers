<?php

class Question extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'questions';

    protected $fillable = array('text');

    public function author() {
    	return $this->belongsTo('User', 'outbox_user_id');
    }

    public function answers() {
    	return $this->hasMany('Answer');
    }

    public function recepient() {
        return $this->belongsTo('User', 'inbox_user_id');
    }
}