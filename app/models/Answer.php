<?php

class Answer extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'answers';

    protected $fillable = array('text');

    public function question() {
    	return $this->belongsTo('Question');
    }

    public function author() {
    	return $this->belongsTo('User', 'user_id');
    }
}