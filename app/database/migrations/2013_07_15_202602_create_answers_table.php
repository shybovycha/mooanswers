<?php

use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('answers', function($table) {
			$table->increments('id');
			$table->integer('question_id');
			$table->integer('user_id');
			$table->text('text');
			$table->timestamps();

			// $table->foreign('question_id')->references('id')->on('questions');
			// $table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('answers');
	}

}