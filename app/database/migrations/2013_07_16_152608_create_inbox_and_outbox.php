<?php

use Illuminate\Database\Migrations\Migration;

class CreateInboxAndOutbox extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('questions', function($table) {
			$table->integer('inbox_user_id');
			$table->renameColumn('user_id', 'outbox_user_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('questions', function($table) {
			$table->dropColumn('inbox_user_id');
			$table->renameColumn('outbox_user_id', 'user_id');
		});
	}

}