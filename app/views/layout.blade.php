<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>MooAnswers</title>

		{{ HTML::style('css/bootstrap.min.css') }}
		{{ HTML::style('css/bootstrap-responsive.min.css') }}
		{{ HTML::style('css/chosen.min.css') }}
		{{ HTML::style('css/custom.css') }}

		{{ HTML::script('js/jquery-2.0.3.min.js') }}
		{{ HTML::script('js/bootstrap.min.js') }}
		{{ HTML::script('js/chosen.jquery.min.js') }}

		@if (isset($user))
			@if (isset($user->page_background))
				<style type="text/css">
					body {
						background: url('{{ asset($user->page_background); }}') no-repeat;
					}
				</style>
			@endif
		@else
			@if (isset(Auth::user()->id) && isset(Auth::user()->page_background))
				<style type="text/css">
					body {
						background: url('{{ asset(Auth::user()->page_background); }}') no-repeat;
					}
				</style>
			@endif
		@endif
	</head>

	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<a class="brand" href="/">MooAnswers</a>

				<ul class="nav">
					@if (!isset(Auth::user()->id))
						<li><a href="{{ action('UsersController@logIn') }}">{{ Lang::get('messages.Sign in') }}</a></li>
						<li><a href="{{ action('UsersController@create') }}">{{ Lang::get('messages.Sign up') }}</a></li>
					@else
						<li><a href="{{ action('UsersController@edit', array('id' => Auth::user()->id)) }}">{{ Auth::user()->name }}</a></li>
						<li><a href="{{ action('UsersController@signOut') }}">{{ Lang::get('messages.Sign out') }}</a></li>
					@endif

					<li><a href="{{ action('QuestionsController@create') }}">{{ Lang::get('messages.Ask a question!') }}</a></li>
				</ul>
			</div>
		</div>

		<div class="container">
			@yield('content')
		</div>
	</body>
</html>