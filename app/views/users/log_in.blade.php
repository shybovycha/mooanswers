@extends('layout')

@section('content')
	@if (isset($errors))
		@foreach ($errors as $error)
			<div class="alert alert-error">{{ $error }}</div>
		@endforeach
	@endif

	{{ Form::open(array('action' => 'UsersController@signIn', 'method' => 'post', 'class' => 'bootstrap-form')) }}
		<h2>{{ Lang::get('messages.Sign in') }}</h2>

		{{ Form::token() }}

		{{ Form::email('email', '', array('placeholder' => Lang::get('messages.Your e-mail address'), 'class' => 'input-block-level')) }}
		{{ Form::password('password', array('placeholder' => Lang::get('messages.Your password'), 'class' => 'input-block-level')) }}

		{{ Form::submit(Lang::get('messages.Sign in'), array('class' => 'btn btn-large btn-primary')) }}

	{{ Form::close() }}
@stop