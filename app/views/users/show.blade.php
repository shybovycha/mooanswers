@extends('layout')

@section('content')
	<h1>{{$user->name}}</h1>

	<img src="{{ asset($user->avatar) }}" alt="avatar" style="max-width: 100px; max-height: 100px;" />

	@if (!is_null(Auth::user()) && ($user->id == Auth::user()->id))
		<h2>{{ Lang::get('messages.Inbox') }}<sup>{{ $user->inbox()->count() }}</sup></h2>

		@foreach ($user->inbox as $question)
			<div class="question">
				{{ $question->text }}
				<br />
				<a href="{{ action('QuestionsController@show', array('id' => $question->id)) }}">{{ Lang::get('messages.More...') }}</a>
				<span>{{ Lang::get('messages.From') }} {{ (isset($question->author) ? $question->author->name : Lang::get('messages.Anonymous')) }}</span>
			</div>
		@endforeach

		<h2>{{ Lang::get('messages.Outbox') }}<sup>{{ $user->outbox()->count() }}</sup></h2>

		@foreach ($user->outbox as $question)
			<div class="question">
				{{ $question->text }}
				<br />
				<a href="{{ action('QuestionsController@show', array('id' => $question->id)) }}">{{ Lang::get('messages.More...') }}</a>
				<span>{{ Lang::get('messages.To') }} {{ $question->recepient->name }}</span>
			</div>
		@endforeach
	@else
		@if (!is_null(Auth::user()))
			@if (is_null(Auth::user()->friends()->find($user->id)))
				<a href="{{ action('UsersController@befriend', array('friend_id' => $user->id)) }}" class="btn btn-primary">{{ Lang::get('messages.Befriend') }}</a>
			@else
				<a href="{{ action('UsersController@unfriend', array('friend_id' => $user->id)) }}" class="btn btn-primary">{{ Lang::get('messages.Unfriend') }}</a>
			@endif
		@endif
	@endif

	@if ($user->friends()->count() > 0)
		<h2>{{ Lang::get('messages.Friends') }}<sup>{{ $user->friends()->count() }}</sup></h2>

		@foreach ($user->friends as $friend)
			<a href="{{ action('UsersController@show', array('id' => $friend->id)) }}">{{ $friend->name }}</a>
		@endforeach
	@endif

	@if ($user->questions()->count() > 0)
		<h2>{{ Lang::get('messages.Questions') }}<sup>{{ $user->questions()->count() }}</sup></h2>

		@foreach ($user->questions as $question)
			<div class="question">
				{{ $question->text }}
				<br />
				<a href="{{ action('QuestionsController@show', array('id' => $question->id)) }}">{{ Lang::get('messages.More...') }}</a>
			</div>
		@endforeach
	@endif

	@if ($user->answers()->count() > 0)
		<h2>{{ Lang::get('messages.Answers') }}<sup>{{ $user->answers()->count() }}</sup></h2>

		@foreach ($user->answers as $answer)
			<div class="answer">
				{{ $answer->text }}
				<br />
				<a href="{{ action('QuestionsController@show', array('id' => $answer->question->id)) }}#answer-{{ $answer->id }}">{{ Lang::get('messages.More...') }}</a>
			</div>
		@endforeach
	@endif
@stop