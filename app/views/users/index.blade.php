@extends('layout')

@section('content')
	@if (count($users) > 0)
		<h1>{{ Lang::get('messages.Already signed') }}</h1>

		@foreach($users as $user)
			<a href="{{ action('UsersController@show', array('id' => $user->id)) }}">{{ $user->name }}</a>
		@endforeach

		<div class="row">&zwnj;</div>

		<div class="alert alert-info">
			<p>{{ Lang::get('messages.Ask one of them') }} <a href="{{ action('QuestionsController@create') }}">{{ Lang::get('messages.a question') }}</a>!</p>
		</div>
	@else
		<h1>{{ Lang::get("messages.No one's here!") }}</h1>

		<div class="row">&zwnj;</div>

		<div class="alert alert-info">
			<p>{{ Lang::get('messages.Be the first one to') }} <a href="{{ action('UsersController@create') }}">{{ Lang::get('messages.sign up') }}</a>!</p>
		</div>
	@endif
@stop