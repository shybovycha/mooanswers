@if ($errors->any())
	@foreach ($errors->all() as $msg)
		<div class="alert alert-error">
			<h4>{{ Lang::get('messages.Error!') }}</h4>

			{{ $msg }}
		</div>
	@endforeach
@endif

@if (isset(Auth::user()->id))
	{{ Form::open(array('action' => array('UsersController@update', Auth::user()->id), 'method' => 'post', 'files' => true, 'class' => 'bootstrap-form')) }}
@else
	{{ Form::open(array('action' => 'UsersController@store', 'method' => 'post', 'files' => true, 'class' => 'bootstrap-form')) }}
@endif
	<h2>{{ Lang::get('messages.Profile settings') }}</h2>

	{{ Form::token() }}

	{{ Form::text('name', $user->name, array('placeholder' => Lang::get('messages.Your nick name'), 'class' => 'input-block-level')) }}
	{{ Form::email('email', $user->email, array('placeholder' => Lang::get('messages.Your e-mail address'), 'class' => 'input-block-level')) }}
	{{ Form::password('password', array('placeholder' => Lang::get('messages.Choose password'), 'class' => 'input-block-level')) }}
	{{ Form::password('password_confirmation', array('placeholder' => Lang::get('messages.Confirm that password'), 'class' => 'input-block-level')) }}

	{{ Form::label('avatar', Lang::get('messages.Choose avatar:')) }}
	{{ Form::file('avatar', array('class' => 'input-block-level')) }}

	@if (isset($user->id))
		<div class="row">&zwnj;</div>

		{{ Form::label('page_background', Lang::get('messages.Choose background for your page:')) }}
		{{ Form::file('page_background', array('class' => 'input-block-level')) }}
	@endif

	<div class="row">&zwnj;</div>

	{{ Form::submit(Lang::get('messages.Save'), array('class' => 'btn btn-primary btn-large')) }}

{{ Form::close() }}