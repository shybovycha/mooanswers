@extends('layout')

@section('content')
	@if (count($users) > 0)
		<h1>{{ Lang::get('messages.Already signed') }}</h1>

		@foreach($users as $user)
			<a href="{{ action('UsersController@show', array('id' => $user->id)) }}">{{ $user->name }}</a>
		@endforeach
	@else
		<h1>{{ Lang::get("messages.No one's here!") }}</h1>
		<p>{{ Lang::get("messages.Be the first one to") }} <a href="{{ action('UsersController@create') }}">{{ Lang::get('messages.sign up') }}</a>!</p>
	@endif
@stop