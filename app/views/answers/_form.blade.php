{{ Form::open(array('action' => array('AnswersController@store', $question->id), 'method' => 'post')) }}

	{{ Form::token() }}

	{{ Form::textarea('text', null, array('placeholder' => Lang::get('messages.Your answer here, please'), 'class' => 'input-block-level')) }}

	{{ Form::submit(Lang::get('messages.Save'), array('class' => 'btn btn-large btn-primary input-block-level')) }}

{{ Form::close() }}