@extends('layout')

@section('content')
	<h1>{{$user->name}}</h1>

	<img src="{{$user->avatar}}" alt="avatar" />

	@if ($user->friends()->count() > 0)
		<h2>{{ Lang::get('messages.Friends') }}<sup>{{ $user->friends()->count() }}</sup></h2>

		@foreach ($user->friends as $friend)
			<a href="{{ action('UsersController@show', array('id' => $friend->id)) }}">{{ $friend->name }}</a>
		@endforeach
	@endif

	@if ($user->questions()->count() > 0)
		<h2>{{ Lang::get('messages.Questions') }}<sup>{{ $user->questions()->count() }}</sup></h2>

		@foreach ($user->questions as $question)
			<div class="question">
				{{ $question->text }}
				<br />
				<a href="{{ action('QuestionsController@show', array('id' => $question->id)) }}">{{ Lang::get('messages.More...') }}</a>
			</div>
		@endforeach
	@endif

	@if ($user->answers()->count() > 0)
		<h2>{{ Lang::get('messages.Answers') }}<sup>{{ $user->answers()->count() }}</sup></h2>

		@foreach ($user->answers as $answer)
			<div class="answer">
				{{ $answer->text }}
				<br />
				<a href="{{ action('QuestionsController@show', array('id' => $answer->question()->id)) }}#answer-{{ $answer->id }}">{{ Lang::get('messages.Show') }}</a>
			</div>
		@endforeach
	@endif
@stop