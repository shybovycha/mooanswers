<script type="text/javascript">
	$(document).ready(function() {
		$("#recepient_id").chosen();
	});
</script>

{{ Form::open(array('action' => 'QuestionsController@store', 'method' => 'post', 'class' => 'bootstrap-form')) }}
	<h2>{{ Lang::get('messages.Ask a question') }}</h2>

	{{ Form::token() }}

	{{ Form::select('recepient_id', $recepients, '', array('id' => 'recepient_id', 'data-placeholder' => Lang::get('messages.Whom would you like to ask?'))) }}

	<div class="row">&zwnj;</div>

	{{ Form::textarea('text', null, array('placeholder' => Lang::get('messages.Your question here'))) }}

	{{ Form::submit(Lang::get('messages.Save'), array('class' => 'btn btn-primary btn-large')) }}

{{ Form::close() }}