@extends('layout')

@section('content')
	<div class="hero-unit">
		<p>{{ $question->text }}</p>
		<p>

			@if (isset(Auth::user()->id))
				<a class="btn btn-primary btn-large" href="#answer">{{ Lang::get('messages.Answer') }}</a>
			@else
				<a class="btn btn-primary btn-large" href="{{ action('UsersController@signIn') }}">{{ Lang::get('messages.Answer') }}</a>
			@endif
		</p>
	</div>

	@if ($question->answers()->count() > 0)
		<h2>Answers<sup>{{ $question->answers()->count() }}</sup></h2>

		@foreach ($question->answers as $answer)
			<div class="media" id="answer-{{ $answer->id }}">
				<a class="pull-left" href="#">
					<img class="media-object" src="{{ asset($answer->author->avatar) }}" alt="{{ Lang::get('messages.avatar') }}" style="max-width: 100px; max-height: 100px;" />
				</a>

				<div class="media-body">
					<h4 class="media-heading"><a href="{{ action('UsersController@show', array('id' => $answer->author->id)) }}">{{ $answer->author->name }}</a></h4>

					{{ $answer->text }}
				</div>
			</div>
		@endforeach
	@endif

	@if (isset(Auth::user()->id))
		<div class="row">&zwnj;</div>
		<div id="answer">
			@include('answers._form')
		</div>
	@endif
@stop
