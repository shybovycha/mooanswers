@extends('layout')

@section('content')
	@if (count($questions) > 0)
		<h1>{{ Lang::get('messages.Questions asked') }}</h1>

		@foreach($questions as $question)
			<div class="question">
				{{ $question->text }}
				<a href="{{ action('QuestionsController@show', array('id' => $question->id)) }}">{{ Lang::get('messages.More...') }}</a>
			</div>
		@endforeach
	@else
		<h1>{{ Lang::get('messages.Nothig was asked...') }}</h1>
		<p>{{ Lang::get('messages.Be the first one to') }} <a href="{{ action('QuestionsController@create') }}">{{ Lang::get('messages.ask') }}</a>!</p>
	@endif
@stop