<?php
	class AnswersController extends BaseController {
		protected $layout = 'layout';

		public function __construct() {
			$this->beforeFilter('answers.filter_params', array('only' => array('create', 'update')));
		}

		public function create($question_id) {
			$answer = new Answer();
			$question = Question::find($question_id);

			return View::make('answers.new', array('answer' => $answer, 'question' => $question));
		}

		public function store($question_id) {
			$params = Input::only('text');

			$user = Auth::user();

			$answer = Answer::create($params);
			$question = Question::find($question_id);

			$answer->question()->associate($question);
			$answer->author()->associate($user);

			$question->answers()->save($answer);
			$user->answers()->save($answer);
			$question->touch();


			return Redirect::action('QuestionsController@show', array('id' => $question_id))->with(array('info' => array(Lang::get('messages.Answer created'))));
		}

		public function edit($id) {
			$answer = Answer::find($id);

			return View::make('answers.edit', array('answer' => $answer));
		}

		public function update($id) {
			$answer = Answer::find($id);

			$params = Input::only('text');
			$answer->update($params);

			return Redirect::action('QuestionsController@show', array('id' => $answer->question->id))->with(array('info' => array(Lang::get('messages.Answer saved successfully'))));
		}
	}