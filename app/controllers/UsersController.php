<?php
	class UsersController extends BaseController {
		protected $layout = 'layout';

		public function __construct() {
			$this->beforeFilter('auth', array('only' => array('edit', 'update')));
			$this->beforeFilter('users.filter_params', array('only' => array('create', 'update')));
		}

		public function index() {
			$users = User::all();

			return View::make('users.index', array('users' => $users));
		}

		public function create() {
			$user = new User();

			return View::make('users.new', array('user' => $user));
		}

		public function store() {
			$params = Input::only('email', 'password', 'name', 'avatar');
			$params['password'] = Hash::make($params['password']);
			$user = User::create($params);
			Auth::loginUsingId($user->id);
			$this->handleAvatar();
			return Redirect::action('UsersController@show', array('id' => $user->id))->with(array('info' => array(Lang::get('messages.Registered!'))));
		}

		public function show($id) {
			$user = User::find($id);

			return View::make('users.show', array('user' => $user));
		}

		public function edit($id) {
			$user = User::find($id);

			return View::make('users.edit', array('user' => $user));
		}

		public function update($id) {
			$user = User::find($id);

			$params = Input::only('email', 'password', 'name', 'avatar');
			$params['password'] = Hash::make($params['password']);
			$user->update($params);

			$this->handleAvatar();
			$this->handlePageBackground();

			if (Auth::user()->id == $id) {
				Auth::logout();
				Auth::loginUsingId($id);
			}

			return Redirect::action('UsersController@show', array('id' => $id))->with(array('info' => array(Lang::get('messages.User saved successfully'))));
		}

		public function befriend($friend_id) {
			$user_id = Auth::user()->id;

			if (!isset($user_id)) {
				return Redirect::action('UsersController@index');
			}

			$user = Auth::user();
			$friend = User::find($friend_id);

			$user->friends()->attach($friend);

			return Redirect::intended(action('UsersController@show', array('id' => Auth::user()->id)));
		}

		public function unfriend($friend_id) {
			$user_id = Auth::user()->id;

			if (!isset($user_id)) {
				return Redirect::action('UsersController@index');
			}

			$user = Auth::user();
			$friend = User::find($friend_id);

			$user->friends()->detach($friend);

			return Redirect::intended(action('UsersController@show', array('id' => Auth::user()->id)));
		}

		public function logIn() {
			return View::make('users.log_in');
		}

		public function signIn() {
			$user_data = array(
				'email' => Input::get('email'),
				'password' => Input::get('password')
			);

			if (Auth::attempt($user_data)) {
				return Redirect::action('UsersController@show', array('id' => Auth::user()->id));
			} else {
				return Redirect::action('UsersController@logIn')->with(array('errors' => array(Lang::get('messages.Logging in failed'))));
			}
		}

		public function signOut() {
			Auth::logout();

			return Redirect::intended('/');
		}

		protected function handleAvatar() {
			if (Input::hasFile('avatar')) {
				$user_id = Auth::user()->id;
				$avatar = Input::file('avatar');

				$extension = $avatar->getClientOriginalExtension();

				$new_avatar_name = 'user_' . $user_id . '.' . $extension;

				$public_subdir = 'users_data/';
				$new_avatar_path = $public_subdir . $new_avatar_name;
				$destination_path = public_path() . '/' . $public_subdir;

				$avatar->move($destination_path, $new_avatar_name);

				Auth::user()->update(array('avatar' => $new_avatar_path));
			}
		}

		protected function handlePageBackground() {
			if (Input::hasFile('page_background')) {
				$user_id = Auth::user()->id;
				$file = Input::file('page_background');

				$extension = $file->getClientOriginalExtension();

				$new_file_name = 'background_' . $user_id . '.' . $extension;

				$public_subdir = 'users_data/';
				$new_file_path = $public_subdir . $new_file_name;
				$destination_path = public_path() . '/' . $public_subdir;

				$file->move($destination_path, $new_file_name);

				$res = Auth::user()->update(array('page_background' => $new_file_path));
			}
		}
	}