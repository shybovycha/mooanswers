<?php
	class QuestionsController extends BaseController {
		protected $layout = 'layout';

		public function index() {
			$questions = Question::all();

			return View::make('questions.index', array('questions' => $questions));
		}

		public function create() {
			$question = new Question();
			$users = User::all();

			$recepients = array('' => '');

			foreach ($users as $user) {
				if (isset(Auth::user()->id) && ($user->id == Auth::user()->id)) {
					continue;
				}

				$recepients[$user->id] = $user->name;
			}

			return View::make('questions.new', array('question' => $question, 'recepients' => $recepients));
		}

		public function store() {
			$params = Input::only('text');
			$recepient_id = Input::get('recepient_id');

			if (!isset($recepient_id)) {
				return Redirect::action('QuestionsController@create')->with(array('errors' => array(Lang::get('messages.You have not specified recepient'))));
			}

			$question = Question::create($params);

			$recepient = User::find($recepient_id);
			$recepient->inbox()->save($question);
			$question->recepient()->associate($recepient);

			$user = Auth::user();

			if (isset($user->id)) {
				$user->outbox()->save($question);
				$question->author()->associate($user);
			}

			return Redirect::action('QuestionsController@show', array('id' => $question->id))->with(array('info' => array(Lang::get('messages.Question created'))));
		}

		public function show($id) {
			$question = Question::find($id);

			return View::make('questions.show', array('question' => $question));
		}

		public function edit($id) {
			$question = Question::find($id);

			return View::make('questions.edit', array('question' => $question));
		}

		public function update($id) {
			$question = Question::find($id);

			$params = Input::only('text');
			$question->update($params);

			return Redirect::action('QuestionsController@show', array('id' => $id))->with(array('info' => array(Lang::get('messages.Question saved successfully'))));
		}
	}